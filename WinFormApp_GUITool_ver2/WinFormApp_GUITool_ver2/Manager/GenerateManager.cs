﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormApp_GUITool_ver2.Manager
{
    /*
        生成管理クラス
     　　ファイル生成、PropertyGridクラスの生成。
    */
    class GenerateManager
    {
        /*
            パラメータクラスのコレクションを生成。
             Formクラスにm_propertyGridを渡す前提
        */
        public void CreateParameters()
        {
            this.m_propertyGrid = new System.Windows.Forms.PropertyGrid();
            // 
            // m_propertyGrid
            // 
            this.m_propertyGrid.LineColor = System.Drawing.SystemColors.ControlDark;
            this.m_propertyGrid.Location = new System.Drawing.Point(12, 12);
            this.m_propertyGrid.Name = "propertyGrid1";
            this.m_propertyGrid.Size = new System.Drawing.Size(260, 237);
            this.m_propertyGrid.TabIndex = 0;

            //
            //param test
            //
            this.m_collection = new Collection();
            //PropertyGridで編集をさせるパラメータの選択（SelectedObjectに渡したm_collectionを編集する形になります）
            this.m_propertyGrid.SelectedObject = m_collection;
        }

        /*
            パラメータをファイル出力
             ファイル保存ダイアログで選択されたファイル名を、
             SaveParameterFileに渡しています。
        */
        public void OutputParameters()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            //はじめのファイル名を指定する
            //はじめに「ファイル名」で表示される文字列を指定する
            saveFileDialog.FileName = ".prm";
            //はじめに表示されるフォルダを指定する
            //saveFileDialog.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            //指定しない（空の文字列）の時は、現在のディレクトリが表示される
            saveFileDialog.Filter = "PRMファイル(*.prm)|*.prm";
            //[ファイルの種類]ではじめに選択されるものを指定する
            //2番目の「すべてのファイル」が選択されているようにする
            saveFileDialog.FilterIndex = 2;
            //タイトルを設定する
            saveFileDialog.Title = "保存先のファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            saveFileDialog.RestoreDirectory = true;
            //既に存在するファイル名を指定したとき警告する
            //デフォルトでTrueなので指定する必要はない
            saveFileDialog.OverwritePrompt = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            saveFileDialog.CheckPathExists = true;

            //ダイアログを表示する
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                //OKボタンがクリックされたとき、選択されたファイル名を表示する
                SaveParameterFile(saveFileDialog.FileName);
            }
            
        }

        /*
            ファイルにパラメータ情報を書き込み
             fileName のファイルに現在のパラメータ情報を書き込みます。
        */
        public void SaveParameterFile(string fileName)
        {
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            //ファイルを開く
            StreamWriter writer = new StreamWriter(fileName, false, sjisEnc);

            //ヘッダー情報を取得
            string fileOutputStr = Parameter.GetHeader();
            //リストに登録されているパラメータの詳細を取得
            for (int i = 0; i < m_collection.ListValue.Count; i++)
            {
                Parameter outPrm = m_collection.ListValue[i];
                fileOutputStr += outPrm.GetValue();
            }
            //取得したヘッダー+パラメータ情報を書き込み
            writer.Write(fileOutputStr);
            writer.Close();
        }

        public System.Windows.Forms.PropertyGrid m_propertyGrid; //プロパティグリッド
        private Collection m_collection; //プロパティグリッドで編集するコレクション
    }
}
