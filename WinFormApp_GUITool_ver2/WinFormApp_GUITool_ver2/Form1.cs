﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormApp_GUITool_ver2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void AddPropertyGrid(System.Windows.Forms.PropertyGrid pg)
        {
            this.Controls.Add(pg);
        }

        private void createParameterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.manager.CreateParameters();
            this.Controls.Add(this.manager.m_propertyGrid);
        }

        private void outputParameterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.manager.OutputParameters();
        }
    }
}
