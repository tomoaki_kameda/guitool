﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormApp_GUITool_ver2
{
    /*
        Prameterクラス
         PropertyGridで管理させるパラメータクラス    
    */
    //[DefaultProperty("StringValue")]  //これを宣言することで、SelectedObjectに設定したクラスが表示される時、
                                        //初期でカーソルで選択されるプロパティを選択出来る
    public class Parameter
    {
        /*
            メンバ変数 
        */
        private int integerValue = 0;
        private string stringValue = "default";
        private bool booleanValue = false;
        private WindowSize sizeValue = new WindowSize(0, 0);

        /*
            ウインドウサイズクラス
             PropertyGridで、自作クラスを管理させる場合のサンプルとして作ったクラス
             （この程度の内容なら、用意されているSizeクラスを使うほうがいい） 
        */
        //自作クラスの値を、１パラメータとして変更するには、ExpandableObjectConverterの派生クラスで手順を踏んだアクセスをさせる関数を用意する必要ある
        public class WindowSize
        {
            private int width = 0;
            private int height = 0;
            public WindowSize(int w, int h)
            {
                width = w;
                height = h;
            }
            public WindowSize(Size size)
            {
                size.Width = size.Width;
                size.Height = size.Height;
            }
            public int Width
            {
                get { return width; }
                set { width = value; }
            }
            public int Height
            {
                get { return height; }
                set { height = value; }
            }
       };
       // ものすごく面倒くさいが、これをしないと自作クラスをプロパティグリッドからはアクセスできない
        public class WindowSizeConverter : ExpandableObjectConverter
        {
            //コンバータがオブジェクトを指定した型に変換できるか
            //（変換できる時はTrueを返す）
            //ここでは、WindowSize型のオブジェクトには変換可能とする
            public override bool CanConvertTo(
                ITypeDescriptorContext context, Type destinationType)
            {
                if (destinationType == typeof(WindowSize))
                    return true;
                return base.CanConvertTo(context, destinationType);
            }

            //指定した値オブジェクトを、指定した型に変換する
            //WindowSize型のオブジェクトをString型に変換する方法を提供する
            public override object ConvertTo(ITypeDescriptorContext context,
                CultureInfo culture, object value, Type destinationType)
            {
                if (destinationType == typeof(string) &&
                    value is WindowSize)
                {
                    WindowSize ws = (WindowSize)value;
                    return ws.Width.ToString() + "," + ws.Height.ToString();
                }
                return base.ConvertTo(context, culture, value, destinationType);
            }

            //コンバータが特定の型のオブジェクトをコンバータの型に変換できるか
            //（変換できる時はTrueを返す）
            //ここでは、Size型のオブジェクトなら変換可能とする
            public override bool CanConvertFrom(
                ITypeDescriptorContext context, Type sourceType)
            {
                if (sourceType == typeof(string))
                    return true;
                return base.CanConvertFrom(context, sourceType);
            }

            //指定した値をコンバータの型に変換する
            //Size型のオブジェクトをWindowSize型に変換する方法を提供する
            public override object ConvertFrom(ITypeDescriptorContext context,
                CultureInfo culture, object value)
            {
                if (value is string)
                {
                    string[] str = value.ToString().Split(new char[] { ',' });
                    WindowSize ws = new WindowSize(int.Parse(str[0]), int.Parse(str[1]));
                    return ws;
                }
                return base.ConvertFrom(context, culture, value);
            }
        }
        
        /*
            getter setter     
        */
        [TypeConverter(typeof(WindowSizeConverter))] //上記で宣言したクラスを、TypeConverterクラスで登録する事で、
                                                     //PropertyGridで自作クラスが編集可能となる。
        public WindowSize WindowSizeValue
        {
            get { return sizeValue; }
            set
            {
                WindowSize ws = (WindowSize)value;
                ws.Width = (ws.Width >= 0 ? ws.Width : 0);
                ws.Height = (ws.Height >= 0 ? ws.Height : 0);
                sizeValue = ws;
            }
        }

        public int IntegerValue { get; set; }

        //[Description("ここにStringValueの説明を書きます。")] // ここで宣言している文字列は、パラメータがカーソルで選択されている際の、説明文として表示される
        public string StringValue { get; set; }

        //[Category("フラグ")] //これで宣言された文字列は、パラメータの表示時に他に同じ文字列でカテゴリ宣言された場合に、まとめて表示される
        public bool BooleanValue { get; set; }


        /*
            パラメータ詳細出力
             文字列形式でパラメータ情報を吐き出す際の関数
             これ自体はパラメータクラスごとに持っておく方がいいと判断
        */
        //ヘッダー出力
        public static string GetHeader()
        {
            string header = "<Parameter>,WindowSize,int,string,bool\n";
            return header;
        }
        //詳細出力
        public string GetValue()
        {
            string value = sizeValue.Width.ToString() + "," +
                           sizeValue.Height.ToString() + "," +
                           integerValue.ToString() + "," +
                           stringValue + "," +
                           booleanValue.ToString() + "\n";
            return value;
        }
    }

    /*
        コレクションクラス
         PropertyGridを使用する際に、List型のメンバを持つクラスを渡す事で、
         リストの登録、削除もPropertyGridで編集可能となる。
    */
    public class Collection
    {
        private List<Parameter> m_list = new List<Parameter>();

        public List<Parameter> ListValue
        {
            get { return m_list; }
            set { m_list = value; }
        }
    }
}
