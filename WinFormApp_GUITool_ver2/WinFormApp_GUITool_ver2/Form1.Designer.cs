﻿using WinFormApp_GUITool_ver2.Manager;

namespace WinFormApp_GUITool_ver2
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.createParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createParameterToolStripMenuItem,
            this.outputParameterToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // createParameterToolStripMenuItem
            // 
            this.createParameterToolStripMenuItem.Name = "createParameterToolStripMenuItem";
            this.createParameterToolStripMenuItem.Size = new System.Drawing.Size(107, 20);
            this.createParameterToolStripMenuItem.Text = "create parameter";
            this.createParameterToolStripMenuItem.Click += new System.EventHandler(this.createParameterToolStripMenuItem_Click);
            // 
            // outputParameterToolStripMenuItem
            // 
            this.outputParameterToolStripMenuItem.Name = "outputParameterToolStripMenuItem";
            this.outputParameterToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.outputParameterToolStripMenuItem.Text = "output parameter";
            this.outputParameterToolStripMenuItem.Click += new System.EventHandler(this.outputParameterToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

            //
            // GenerateManager
            //
            this.manager = new GenerateManager();
        }

        #endregion

        private GenerateManager manager;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem createParameterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputParameterToolStripMenuItem;
    }
}

