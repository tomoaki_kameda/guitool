﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormApp_GUITool
{
    public class ParamGroup : ParamBase
    {
        GroupBox m_groupPage = new GroupBox();    //1パラメータのグループ
        ComboBox m_selectParam = new ComboBox();    //1パラメータの数値型を変更するオブジェクト
        ParamBase m_inputParam;                       //数値型変更による、パラメータ入力オブジェクト

        public enum EParamType : int
        {
            EInteger,
            EFloat,
            EString,
            EParamMax
        };

        public static string[] paramNameStr = new string[(int)EParamType.EParamMax] {"Integer","Float","String"};

        public ParamGroup(EParamType type)
        {
            m_selectParam.FormattingEnabled = true;
            m_selectParam.Items.AddRange(new object[]
            {
               paramNameStr[(int)EParamType.EInteger],
               paramNameStr[(int)EParamType.EFloat],
               paramNameStr[(int)EParamType.EString]
            }
            );
            m_selectParam.Location  = new System.Drawing.Point(190, 18);
            m_selectParam.Name      = "SelectParam";
            m_selectParam.Size      = new System.Drawing.Size(68, 20);
            m_selectParam.TabIndex  = 0;
            m_selectParam.TextChanged += new System.EventHandler(this.paramBox_TextChanged);
            m_selectParam.SelectedIndex = 0;

            m_groupPage.Controls.Add(m_selectParam);
            m_groupPage.Location = new System.Drawing.Point(6, 6);
            m_groupPage.Name = "GroupPage";
            m_groupPage.Size = new System.Drawing.Size(264, 56);
            m_groupPage.TabIndex = 0;
            m_groupPage.TabStop = false;
            m_groupPage.Text = "Param";

            //初期選択IDから入力パラメータオブジェクトの変更をする
            m_selectParam.SelectedIndex = (int)type;
            changeInputParam(type);
        }

        public override Control GetContorlItem()
        {
            return m_groupPage;
        }

        private void paramBox_TextChanged(object sender, EventArgs e)
        {
            ComboBox param = (ComboBox)sender;
            changeInputParam((EParamType)param.SelectedIndex);
        }

        private void changeInputParam(EParamType type )
        {
            switch(type)
            {
            case EParamType.EInteger:
                m_inputParam = NnmberFactory.CreateNumber(NnmberFactory.ENumberType.EInteger);

                break;
            case EParamType.EFloat:
                m_inputParam = NnmberFactory.CreateNumber(NnmberFactory.ENumberType.EFloat);

                break;
            case EParamType.EString:
                    m_inputParam = TextFactory.CreateText();

                break;
            }
            Control inputParam = m_inputParam.GetContorlItem();
            m_groupPage.Controls.Clear();
            m_groupPage.Controls.Add(m_selectParam);
            m_groupPage.Controls.Add(inputParam);
        }

        public void AddLocation( int x, int y )
        {
            m_groupPage.Location = new System.Drawing.Point(m_groupPage.Location.X + x,
                                                            m_groupPage.Location.Y + y);
        }

        public EParamType GetParamType()
        {
            return (EParamType)m_selectParam.SelectedIndex;
        }

        public ParamBase GetInputParam()
        {
            return m_inputParam;
        }
    }
}
