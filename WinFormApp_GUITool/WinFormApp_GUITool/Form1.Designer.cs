﻿namespace WinFormApp_GUITool
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addParentParamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addChildParamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outputParamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.loadParamSheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu1ToolStripMenuItem,
            this.outputParamToolStripMenuItem,
            this.loadParamSheetToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(498, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menu1ToolStripMenuItem
            // 
            this.menu1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addParentParamToolStripMenuItem,
            this.addChildParamToolStripMenuItem});
            this.menu1ToolStripMenuItem.Name = "menu1ToolStripMenuItem";
            this.menu1ToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.menu1ToolStripMenuItem.Text = "add new item";
            // 
            // addParentParamToolStripMenuItem
            // 
            this.addParentParamToolStripMenuItem.Name = "addParentParamToolStripMenuItem";
            this.addParentParamToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.addParentParamToolStripMenuItem.Text = "add parent param";
            this.addParentParamToolStripMenuItem.Click += new System.EventHandler(this.addParentParamToolStripMenuItem_Click);
            // 
            // addChildParamToolStripMenuItem
            // 
            this.addChildParamToolStripMenuItem.Name = "addChildParamToolStripMenuItem";
            this.addChildParamToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.addChildParamToolStripMenuItem.Text = "add child param";
            this.addChildParamToolStripMenuItem.Click += new System.EventHandler(this.addChildParamToolStripMenuItem_Click);
            // 
            // outputParamToolStripMenuItem
            // 
            this.outputParamToolStripMenuItem.Name = "outputParamToolStripMenuItem";
            this.outputParamToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.outputParamToolStripMenuItem.Text = "output param";
            this.outputParamToolStripMenuItem.Click += new System.EventHandler(this.outputParamToolStripMenuItem_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(276, 209);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(498, 383);
            this.tabControl1.TabIndex = 1;
            // 
            // loadParamSheetToolStripMenuItem
            // 
            this.loadParamSheetToolStripMenuItem.Name = "loadParamSheetToolStripMenuItem";
            this.loadParamSheetToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.loadParamSheetToolStripMenuItem.Text = "load param sheet";
            this.loadParamSheetToolStripMenuItem.Click += new System.EventHandler(this.loadParamSheetToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 409);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu1ToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStripMenuItem addParentParamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addChildParamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outputParamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadParamSheetToolStripMenuItem;
    }
}

