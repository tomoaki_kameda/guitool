﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormApp_GUITool
{
    static public class GroupFactory
    {
        static public ParamGroup CreateGroup(ParamGroup.EParamType type)
        {
            ParamGroup paramGroup = new ParamGroup(type);

            return paramGroup;
        }
    }
}
