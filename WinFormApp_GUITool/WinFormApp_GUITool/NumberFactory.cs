﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormApp_GUITool
{
    public class NnmberFactory
    {
        public enum ENumberType
        {
            EInteger,
            EFloat,
            ETypeMax,
        };

        static public NumberBase CreateNumber(ENumberType type)
        {
            NumberBase number = new NumberBase(type);

            return number;
        }
    }
}
