﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static WinFormApp_GUITool.NnmberFactory;

namespace WinFormApp_GUITool
{
    public class NumberBase : ParamBase
    {
        NumericUpDown m_number = new NumericUpDown();

        const int DECIMAL_PLACES    = 3;   //少数有効桁数
        const decimal FLOAT_MAX     = 100.0m;
        const decimal FLOAT_MIN     = -100.0m;
        const decimal FLOAT_INCREMENT = 0.001m;
        const decimal INTEGER_MAX       = 1000;
        const decimal INTEGER_MIN       = -1000;
        const decimal INTEGER_INCREMENT = 1;

        public NumberBase( ENumberType type )
        {
            m_number.Location = new System.Drawing.Point(6, 18);
            m_number.Name = "Number";
            m_number.Size = new System.Drawing.Size(100, 20);

            switch (type)
            {
                case ENumberType.EInteger:
                    m_number.DecimalPlaces = 0;
                    m_number.Maximum = INTEGER_MAX;
                    m_number.Minimum = INTEGER_MIN;
                    m_number.Increment = INTEGER_INCREMENT;
                    break;
                case ENumberType.EFloat:
                    m_number.DecimalPlaces = DECIMAL_PLACES;
                    m_number.Maximum = FLOAT_MAX;
                    m_number.Minimum = FLOAT_MIN;
                    m_number.Increment = FLOAT_INCREMENT;
                    break;
                default:
                    break;
            }


        }

        public override Control GetContorlItem()
        {
           return m_number;
        }
    }
}
