﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormApp_GUITool
{
    public class TextBase : ParamBase
    {
        TextBox m_textBox = new TextBox();

        public TextBase()
        {
            m_textBox.Location = new System.Drawing.Point(6, 18);
            m_textBox.Name = "textBox";
            m_textBox.Size = new System.Drawing.Size(100, 20);
            m_textBox.TabIndex = 0;
        }

        public override Control GetContorlItem()
        {
            return m_textBox;
        }
    }
}
