﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormApp_GUITool
{
    public partial class Form1 : Form
    {
        private ArrayList m_TabArray = new ArrayList();
        private ArrayList m_ParamArray = new ArrayList();

        const int PARAM_GROUP_HEIGHT = 60;
        const string defaultFileName = "output.txt";

        public Form1()
        {
            InitializeComponent();
        }
        public void ResetArray()
        {
            m_ParamArray.Clear();
            if (m_TabArray.Count != 0)
            {
                TabBase first = (TabBase)m_TabArray[0];
                first.Controls.Clear();
            }
        }

        private void addTabPage(TabBase tab)
        {
            m_TabArray.Add(tab);
            this.tabControl1.Controls.Add(tab);
        }
        private void addParamGroup(ParamGroup param)
        {
            if (m_TabArray.Count == 0)
            {
                TabBase tab = TabFactory.CreateTab();
                addTabPage(tab);
                addParamGroup(param);
            }
            else
            {
                param.AddLocation(0, m_ParamArray.Count * PARAM_GROUP_HEIGHT);
                m_ParamArray.Add(param);
                TabBase first = (TabBase)m_TabArray[0];
                first.Controls.Add(param.GetContorlItem());
            }
        }

        private void addParentParamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TabBase tab = TabFactory.CreateTab();
            addTabPage(tab);
        }

        private void addChildParamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ParamGroup param = GroupFactory.CreateGroup(ParamGroup.EParamType.EInteger);
            addParamGroup(param);
        }

        private void outputParamToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            //はじめのファイル名を指定する
            //はじめに「ファイル名」で表示される文字列を指定する
            saveFileDialog.FileName = ".prm";
            //はじめに表示されるフォルダを指定する
            //saveFileDialog.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            //指定しない（空の文字列）の時は、現在のディレクトリが表示される
            saveFileDialog.Filter = "PRMファイル(*.prm)|*.prm";
            //[ファイルの種類]ではじめに選択されるものを指定する
            //2番目の「すべてのファイル」が選択されているようにする
            saveFileDialog.FilterIndex = 2;
            //タイトルを設定する
            saveFileDialog.Title = "保存先のファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            saveFileDialog.RestoreDirectory = true;
            //既に存在するファイル名を指定したとき警告する
            //デフォルトでTrueなので指定する必要はない
            saveFileDialog.OverwritePrompt = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            saveFileDialog.CheckPathExists = true;

            //ダイアログを表示する
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                //OKボタンがクリックされたとき、選択されたファイル名を表示する
                SaveParameterFile(saveFileDialog.FileName);
            }
        }

        private void SaveParameterFile(string fileName)
        {
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            StreamWriter writer = new StreamWriter(fileName, false, sjisEnc);
            string fileOutputStr = "";

            for (int i = 0; i < m_ParamArray.Count; i++)
            {
                ParamGroup param = (ParamGroup)m_ParamArray[i];
                ParamGroup.EParamType type = param.GetParamType();
                ParamBase inputParam = param.GetInputParam();
                //@todo パラメータの種類別に、格納しているパラメータが違うので、統一かここにswitchで分岐させるより良い書き方が無いか考え中
                string sParam;
                int iParam;
                float fParam;
                NumericUpDown numeric;
                TextBox textBox;
                string typeStr = ParamGroup.paramNameStr[(int)type];
                fileOutputStr += typeStr;
                fileOutputStr += ",";
                switch (type)
                {
                    case ParamGroup.EParamType.EInteger:
                        numeric = (NumericUpDown)inputParam.GetContorlItem();
                        iParam = (int)numeric.Value;
                        fileOutputStr += iParam;
                        break;
                    case ParamGroup.EParamType.EFloat:
                        numeric = (NumericUpDown)inputParam.GetContorlItem();
                        fParam = (float)numeric.Value;
                        fileOutputStr += fParam;
                        break;
                    case ParamGroup.EParamType.EString:
                        textBox = (TextBox)inputParam.GetContorlItem();
                        sParam = textBox.Text;
                        fileOutputStr += sParam;
                        break;
                    default:
                        break;
                }

                fileOutputStr += "\n";
            }

            writer.Write(fileOutputStr);

            writer.Close();
        }

        private void loadParamSheetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //読み込むファイル選択用のダイアログクラスを生成
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //はじめのファイル名を指定する
            //はじめに「ファイル名」で表示される文字列を指定する
            openFileDialog.FileName = ".prm";

            //はじめに表示されるフォルダを指定する
            //指定しない（空の文字列）の時は、現在のディレクトリが表示される
            //openFileDialog.InitialDirectory = @"C:\";

            //[ファイルの種類]に表示される選択肢を指定する
            //指定しないとすべてのファイルが表示される
            openFileDialog.Filter = "すべてのPRMファイル(*.prm*)|*.prm*";

            //[ファイルの種類]ではじめに選択されるものを指定する
            //2番目の「すべてのファイル」が選択されているようにする
            openFileDialog.FilterIndex = 2;

            //タイトルを設定する
            openFileDialog.Title = "開くファイルを選択してください";

            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            openFileDialog.RestoreDirectory = true;

            //存在しないファイルの名前が指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            openFileDialog.CheckFileExists = true;

            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            openFileDialog.CheckPathExists = true;

            //ダイアログを表示する
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //OKボタンがクリックされたとき、選択されたファイル名を表示する
                Console.WriteLine(openFileDialog.FileName);
                LoadParameterFile(openFileDialog.FileName);
            }
        }

        private void LoadParameterFile(string fileStr)
        {
            StreamReader reader = new StreamReader(fileStr, Encoding.Default);
            //パラメータ改行（１パラメータ）ごと抜き出して、読んだシートの情報を解析
            {
                if (reader.Peek() != -1) { ResetArray(); }

                while (reader.Peek() > -1)
                {
                    string str = reader.ReadLine();
                    int idx = str.IndexOf(',');
                    string typeStr = str.Substring(0, idx);
                    ParamGroup.EParamType retType = ParamGroup.EParamType.EInteger;
                    ParamGroup param = null;
                    ParamBase inputParam = null;
                    NumericUpDown numeric = null;
                    for (int i = 0; i < (int)ParamGroup.EParamType.EParamMax; i++)
                    {
                        if( typeStr == ParamGroup.paramNameStr[i])
                        {
                            string outStr = str.Substring(idx + 1);
                            switch (i)
                            {
                                case (int)ParamGroup.EParamType.EInteger:
                                    retType = ParamGroup.EParamType.EInteger;
                                    param = GroupFactory.CreateGroup(retType);
                                    inputParam = param.GetInputParam();
                                    numeric = (NumericUpDown)inputParam.GetContorlItem();
                                    numeric.Value = Convert.ToInt32(outStr);
                                    break;
                                case (int)ParamGroup.EParamType.EFloat:
                                    retType = ParamGroup.EParamType.EFloat;
                                    param = GroupFactory.CreateGroup(retType);
                                    inputParam = param.GetInputParam();
                                    numeric = (NumericUpDown)inputParam.GetContorlItem();
                                    numeric.Value = (decimal)Convert.ToDouble(outStr);
                                    break;
                                case (int)ParamGroup.EParamType.EString:
                                    retType = ParamGroup.EParamType.EString;
                                    param = GroupFactory.CreateGroup(retType);
                                    inputParam = param.GetInputParam();
                                    TextBox textBox = (TextBox)inputParam.GetContorlItem();
                                    textBox.Text = outStr;
                                    break;
                            }
                            
                            addParamGroup(param);
                        }
                    }
                   
                }
            }

            reader.Close();
        }
    }
}
